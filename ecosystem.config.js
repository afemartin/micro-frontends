module.exports = {
  apps: [
    // {
    //   name: 'API Checkout',
    //   cwd: 'packages/apis/checkout',
    //   script: 'yarn start',
    //   port: 3021,
    // },
    // {
    //   name: 'API Inspire',
    //   cwd: 'packages/apis/inspire',
    //   script: 'yarn start',
    //   port: 3022,
    // },
    // {
    //   name: 'API Product',
    //   cwd: 'packages/apis/product',
    //   script: 'yarn start',
    //   port: 3020,
    // },
    {
      name: 'API Store (GraphQL)',
      cwd: 'packages/apis/store',
      script: 'yarn start',
      port: 3030,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
    },
    {
      name: 'APP Store',
      cwd: 'packages/apps/store',
      script: 'yarn start',
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      port: 3000,
      env: {
        NODE_ENV: 'development',
      },
    },
    {
      name: 'CDN Images',
      cwd: 'packages/cdn/images',
      script: 'yarn start',
      port: 3003,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      env: {
        NODE_ENV: 'development',
      },
    },
    {
      name: 'MFE Catalog',
      cwd: 'packages/frontends/catalog',
      watch: ['src'],
      ignore_watch: ['node_modules'],
      script: 'yarn dev',
      port: 3015,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      env: {
        NODE_ENV: 'development',
      },
    },
    {
      name: 'MFE Checkout',
      cwd: 'packages/frontends/checkout',
      watch: ['src'],
      ignore_watch: ['node_modules'],
      script: 'yarn dev',
      port: 3011,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      env: {
        NODE_ENV: 'development',
      },
    },
    {
      name: 'MFE Home',
      cwd: 'packages/frontends/home',
      watch: ['src'],
      ignore_watch: ['node_modules'],
      script: 'yarn dev',
      port: 3013,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      env: {
        NODE_ENV: 'development',
      },
    },
    {
      name: 'MFE Inspire',
      cwd: 'packages/frontends/inspire',
      watch: ['src'],
      ignore_watch: ['node_modules'],
      script: 'yarn dev',
      port: 3012,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      env: {
        NODE_ENV: 'development',
      },
    },
    {
      name: 'MFE Navigation',
      cwd: 'packages/frontends/navigation',
      watch: ['src'],
      ignore_watch: ['node_modules'],
      script: 'yarn dev',
      port: 3014,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      env: {
        NODE_ENV: 'development',
      },
    },
    {
      name: 'MFE Product',
      cwd: 'packages/frontends/product',
      watch: ['src'],
      ignore_watch: ['node_modules'],
      script: 'yarn dev',
      port: 3010,
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      env: {
        NODE_ENV: 'development',
      },
    },
  ],
};
