# Micro Frontends

This project its meant to be used to experiment with the concept and possible implementations of micro frontends apps.

## Pre-requirements

This projects uses [yarn workspaces](https://yarnpkg.com/en/docs/workspaces) to handle the monorepo structure of the project.

This project uses [pm2](https://pm2.io/runtime/) to handle multiple processes running in parallel to reproduce a real micro frontends app environment.

## Installation

```bash
yarn
```

## Development

```bash
yarn start
```

## Influence

https://micro-frontends.org

https://medium.com/@_rchaves_/building-microfrontends-part-i-creating-small-apps-710d709b48b7

https://www.mosaic9.org

## Schema

### Web apps:

- Store: http://localhost:3000

### CDN services:

- Image: http://localhost:3003

### Frontend services:

- Product: http://localhost:3010
- Checkout: http://localhost:3011
- Inspire: http://localhost:3012
- Home: http://localhost:3013
- Navigation: http://localhost:3014
- Catalog: http://localhost:3015

### API services:

#### Internal APIs

- Product: http://localhost:3020
- Checkout: http://localhost:3021
- Inspire: http://localhost:3022

#### External APIs

- Store: http://localhost:3030 (GraphQL)

### Documentation

- [Introduction](docs/Introduction.md)
