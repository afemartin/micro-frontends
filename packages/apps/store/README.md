# Store

This is the store app for customers.
 
It will take care of high level layers of routing, composing the site using the frontend elements and allowing them to communicate via event bus.
