import openBrowser from '@tools/dev-utils/openBrowser';
import express from 'express';
import createLocaleMiddleware from 'express-locale';
import ESI from 'nodesi';
import nunjucks from 'nunjucks';
import path from 'path';

const app = express();

// Set Nunjucks template engine
nunjucks
  .configure(path.join(__dirname, 'views'), {
    autoescape: true,
    express: app,
  })
  .addGlobal('NODE_ENV', process.env.NODE_ENV);

// Enable ESI
app.use(ESI.middleware());

// Enable locale detection
app.use(
  createLocaleMiddleware({
    allowed: ['en-GB', 'es-ES', 'fr-FR', 'it-IT'],
    default: 'en-GB',
    priority: ['accept-language', 'default'],
  })
);

// Page routes
app.get(
  `/:locale(en-gb|es-es|fr-fr|it-it)?/`,
  (req: any, res: any): void => {
    if (!req.params.locale) {
      res.redirect(`/${req.locale.toString().toLowerCase()}${req.originalUrl}`);
    }
    const [language, country] = req.params.locale.split('-');
    const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
    res.render('pages/home.njk', { locale });
  }
);
app.get(
  `/:locale(en-gb|es-es|fr-fr|it-it)?/category/:category(road|mountain|city)`,
  (req: any, res: any): void => {
    if (!req.params.locale) {
      res.redirect(`/${req.locale.toString().toLowerCase()}${req.originalUrl}`);
    }
    const [language, country] = req.params.locale.split('-');
    const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
    res.render('pages/catalog.njk', { locale, category: req.params.category });
  }
);
app.get(
  `/:locale(en-gb|es-es|fr-fr|it-it)?/product/:sku?`,
  (req: any, res: any): void => {
    if (!req.params.locale) {
      res.redirect(`/${req.locale.toString().toLowerCase()}${req.originalUrl}`);
    }
    const [language, country] = req.params.locale.split('-');
    const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
    res.render('pages/product.njk', { locale, sku: req.params.sku });
  }
);

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'public')));

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`🚀 Store App listening on port ${port}!`));

if (process.env.NODE_ENV === 'development') {
  setTimeout(() => openBrowser(`http://localhost:${port}`), 5000);
}
