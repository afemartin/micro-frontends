# Images

This is the images CDN which will be used to serve all the static images of the site that will be provided by the APIs.

The initial implementation will be a simple express server providing the static assets in a way to mock the real behaviour of a CDN.
