import express from 'express';
import path from 'path';

const app = express();

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'public')));

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`🚀 CDN listening on port ${port}!`));
