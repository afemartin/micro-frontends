import React from 'react';

import translations from '@resources/translations';

// tslint:disable-next-line:interface-name
interface I18nProviderProps {
  children: any;
  language: string;
}

// tslint:disable-next-line:interface-name
interface I18nProps {
  id: string;
  params?: React.ReactNode[];
}

const I18nContext = React.createContext({});

const I18nProvider: React.FC<I18nProviderProps> = ({ children, language }) => {
  return React.createElement(
    I18nContext.Provider,
    { value: translations[language] },
    children
  );
};

const I18n: React.FC<I18nProps> = ({ id, params }) => {
  const ctx = React.useContext(I18nContext);
  let label = ctx[id];
  if (label && params) {
    params.forEach((param, index) => {
      label = label.replace(`{${index}}`, param);
    });
  }
  return label || null;
};

const getI18nText = (id: string, params?: string[]) => {
  const ctx = React.useContext(I18nContext);
  let label = ctx[id];
  if (label && params) {
    params.forEach((param, index) => {
      label = label.replace(`{${index}}`, param);
    });
  }
  return label || null;
};

export { I18nProvider, getI18nText };
export default I18n;
