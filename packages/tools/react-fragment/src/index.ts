import querystring from 'querystring';

import React from 'react';

// Types
interface FragmentProps {
  locale: string;
  team: string;
  name: string;
  params?: any;
}

const Fragment: React.FC<FragmentProps> = ({ locale, team, name, params }) => {
  const domain =
    {
      checkout: 'http://localhost:3011',
      inspire: 'http://localhost:3012',
      product: 'http://localhost:3010',
    }[team] || null;

  if (domain === null) {
    return null;
  }

  const customElement = `${team}-${name}`;
  const fragmentUrl = `${domain}/${locale.toLowerCase()}/${name}?${querystring.stringify(
    params
  )}`;
  const esiHTML = `<esi:includes src="${fragmentUrl}" />`;

  return React.createElement(customElement, {
    dangerouslySetInnerHTML: { __html: esiHTML },
    ...params,
  });
};

export default Fragment;
