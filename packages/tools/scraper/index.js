const fetch = require('node-fetch');
const fs = require('fs');

const getRandomIntInclusive = (min, max) => {
  // The maximum is inclusive and the minimum is inclusive
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const scrapeUrl = (url, scrapeHtml) => {
  // Generate safe cache file name based on the given url
  const file = `.scrape_cache/${url.replace(/[\:\/\.]/gi, '-')}`;

  return new Promise((resolve) => {
    fs.readFile(file, 'utf8', async (err, data) => {
      if (err) {
        console.log('☁️ Fetching data from:', url);

        const response = await fetch(url);
        const fetchedHtml = await response.text();

        await sleep(getRandomIntInclusive(3000, 8000));

        // Write cache file
        fs.mkdir('.scrape_cache', () => {});
        fs.writeFile(file, fetchedHtml, 'utf8', (err) => {
          if (err) {
            throw err;
          }
        });

        const scrapeData = await scrapeHtml(fetchedHtml);

        if (!scrapeData) {
          console.log('❗️ Data not found');
        }

        return resolve(scrapeData);
      }

      console.log('📔 Fetching data from:', file);

      const cachedHtml = data.toString();

      const scrapeData = scrapeHtml(cachedHtml);

      if (!scrapeData) {
        console.log('❗️ Data not found');
      }

      return resolve(scrapeData);
    });
  });
};

const scrapeUrls = async (urls, scrapeHtml) => {
  const data = [];
  for (let i = 0; i < urls.length; i++) {
    const url = urls[i];
    const scrapeData = await scrapeUrl(url, scrapeHtml);
    if (scrapeData) {
      data.push(scrapeData);
    }
  }
  return data;
};

module.exports = {
  scrapeUrls,
  scrapeUrl,
};
