const cheerio = require('cheerio');

const downloadImage = require('./download-image');

const getCategoryFromUrl = (url) => {
  const translatedCategory = url.split('/')[5];
  switch (translatedCategory) {
    case 'road':
    case 'carretera':
    case 'route':
    case 'strada':
      return 'road';
    case 'mountain':
    case 'montana':
    case 'montagne':
    case 'montagna':
      return 'mountain';
    case 'urban':
    case 'urbanas':
    case 'ville':
    case 'da-citta':
      return 'city';
    default:
      return null;
  }
};

const scrapeHtml = async (html) => {
  const $ = cheerio.load(html);

  const sku = $('.main .images .info span[itemprop=productID]').text();

  if (!sku) return null;

  const url = $('meta[property="og:url"]').attr('content');

  const country = url.substring(22, 24).toUpperCase();
  const language = url.substring(25, 27);

  const product = {
    id: sku,
    name: $('.main .images .info span[itemprop=name]').text(),
    image: null,
    details: {
      category: getCategoryFromUrl(url),
      brand: 'Orbea',
      family: $('.main .images .info .breadcrumbs .familia')
        .text()
        .toUpperCase(),
      model: $('meta[property="og:title"]')
        .attr('content')
        .replace(
          $('.main .images .info .breadcrumbs .familia')
            .text()
            .toUpperCase(),
          ''
        )
        .trim(),
      year: null,
      weight: null,
    },
    price: {
      amount: Number.parseFloat(
        $('.buy .buttons .price span[itemprop=price]').attr('content')
      ),
      currency: $('.buy .buttons .price span[itemprop=priceCurrency]').attr(
        'content'
      ),
    },
    components: $('.content .spec-data ul.components>li')
      .map((i, component) => {
        return {
          name: $(component)
            .find('.option .tipo')
            .text()
            .trim(),
          description: $(component)
            .find('.component')
            .text()
            .trim(),
        };
      })
      .get(),
    colors: await Promise.all(
      $('.main .images .colors-nav ul.colors>li.color')
        .map(async (i, color) => {
          const csku = `${sku}-${$(color)
            .find('img')
            .attr('alt')}`;
          return {
            id: csku,
            name: $(color)
              .find('label')
              .attr('title')
              .trim(),
            image: await downloadImage(
              csku,
              `https:${$('.buy .contents ul.buy-sizes-colors>li .img img')
                .eq(i)
                .attr('src')}`.replace('list', 'zoom')
            ),
            sizes: $('.buy .contents ul.buy-sizes-colors>li .sizes')
              .eq(i)
              .find('ul.size-color>li')
              .map((i, size) => {
                const ssku = `${csku}-${$(size)
                  .find('.size')
                  .attr('data-size')}`;
                return {
                  id: ssku,
                  size: $(size)
                    .find('.size')
                    .attr('data-size'),
                  stock: $(size).attr('class') === 'available',
                };
              })
              .get(),
          };
        })
        .get()
    ),
    url,
    metadata: {
      country,
      language,
    },
  };

  // Set the main product image as the first color image
  product.image = { ...product.colors[0].image };

  return product;
};

module.exports = scrapeHtml;
