const categories = ['road', 'mountain', 'city'];

const markets = [
  {
    locale: 'en-GB',
    domain: {
      road: 'https://www.orbea.com/gb-en/bicycles/road',
      mountain: 'https://www.orbea.com/gb-en/bicycles/mountain',
      city: 'https://www.orbea.com/gb-en/bicycles/urban',
    },
  },
  {
    locale: 'es-ES',
    domain: {
      road: 'https://www.orbea.com/es-es/bicicletas/carretera',
      mountain: 'https://www.orbea.com/es-es/bicicletas/montana',
      city: 'https://www.orbea.com/es-es/bicicletas/urbanas',
    },
  },
  {
    locale: 'fr-FR',
    domain: {
      road: 'https://www.orbea.com/fr-fr/velos/route',
      mountain: 'https://www.orbea.com/fr-fr/velos/montagne',
      city: 'https://www.orbea.com/fr-fr/velos/ville',
    },
  },
  {
    locale: 'it-IT',
    domain: {
      road: 'https://www.orbea.com/it-it/biciclette/strada',
      mountain: 'https://www.orbea.com/it-it/biciclette/montagna',
      city: 'https://www.orbea.com/it-it/biciclette/da-citta',
    },
  },
];

const resources = {
  road: [
    'avant/cat/avant-h30',
    'avant/cat/avant-h40',
    'avant/cat/avant-h50',
    'avant/cat/avant-h60',
    'avant/cat/avant-m20team-d',
    'avant/cat/avant-m30team-d',
    'avant/cat/avant-m40team-d',
    'orca-aero/cat/orca-aero-m10-team-d',
    'orca-aero/cat/orca-aero-m11-team-d',
    'orca-aero/cat/orca-aero-m20-team',
    'orca-aero/cat/orca-aero-m20-team-d',
    'orca-aero/cat/orca-aero-m20team',
    'orca-aero/cat/orca-aero-m20team-d',
    'orca-aero/cat/orca-aero-m20team-pwr',
    'orca-aero/cat/orca-aero-m21-team-d',
    'orca-aero/cat/orca-aero-m25team-d',
    'orca-aero/cat/orca-aero-m30team',
    'orca-aero/cat/orca-aero-m30team-d',
    'orca/cat/orca-m10-ltd-d',
    'orca/cat/orca-m10-team-d',
    'orca/cat/orca-m11-ltd-d',
    'orca/cat/orca-m11-team-d',
    'orca/cat/orca-m20',
    'orca/cat/orca-m20-ltd-d',
    'orca/cat/orca-m20-team',
    'orca/cat/orca-m20-team-d',
    'orca/cat/orca-m20ltd-d',
    'orca/cat/orca-m20team',
    'orca/cat/orca-m20team-d',
    'orca/cat/orca-m20team-pwr',
    'orca/cat/orca-m21-ltd-d',
    'orca/cat/orca-m21-team-d',
    'orca/cat/orca-m21-team-d-20',
    'orca/cat/orca-m21-team-d-l-20',
    'orca/cat/orca-m25team-d',
    'orca/cat/orca-m30',
    'orca/cat/orca-m30team',
    'orca/cat/orca-m30team-d',
    'orca/cat/orca-m40',
    'terra/cat/terra-h30-d',
    'terra/cat/terra-h30-d-1x',
    'terra/cat/terra-h40-d',
    'terra/cat/terra-m20-d-1x',
    'terra/cat/terra-m30-d',
    'terra/cat/terra-m30-d-1x',
  ],
  mountain: [
    'alma/cat/alma-29-h20',
    'alma/cat/alma-29-h20-eagle',
    'alma/cat/alma-29-h30',
    'alma/cat/alma-29-h50',
    'alma/cat/alma-29-m-ltd',
    'alma/cat/alma-29-m-team',
    'alma/cat/alma-29-m10',
    'alma/cat/alma-29-m15',
    'alma/cat/alma-29-m25',
    'alma/cat/alma-29-m30',
    'alma/cat/alma-29-m50',
    'alma/cat/alma-29-m50-eagle',
    'laufey/cat/laufey-h-ltd',
    'laufey/cat/laufey-h10',
    'laufey/cat/laufey-h30',
    'mx/cat/mx-29-20',
    'mx/cat/mx-29-30',
    'mx/cat/mx-29-40',
    'mx/cat/mx-29-50',
    'mx/cat/mx-29-ent-40',
    'mx/cat/mx-29-ent-50',
    'occam/cat/occam-h10',
    'occam/cat/occam-h20',
    'occam/cat/occam-h20-eagle',
    'occam/cat/occam-h30',
    'occam/cat/occam-m-ltd',
    'occam/cat/occam-m10',
    'occam/cat/occam-m30',
    'occam/cat/occam-m30-eagle',
    'oiz/cat/oiz-29-h10',
    'oiz/cat/oiz-29-h20',
    'oiz/cat/oiz-29-h30',
    'oiz/cat/oiz-29-m-ltd',
    'oiz/cat/oiz-29-m-team',
    'oiz/cat/oiz-29-m10',
    'oiz/cat/oiz-29-m10-tr',
    'oiz/cat/oiz-29-m20-tr',
    'oiz/cat/oiz-29-m30',
    'rallon/cat/rallon-m-ltd',
    'rallon/cat/rallon-m-team',
    'rallon/cat/rallon-m10',
    'rallon/cat/rallon-m20',
  ],
  city: [
    'carpe/cat/carpe-10',
    'carpe/cat/carpe-20',
    'carpe/cat/carpe-25',
    'carpe/cat/carpe-30',
    'carpe/cat/carpe-40',
    'katu/cat/katu-20',
    'katu/cat/katu-40',
    'optima/cat/optima-a20',
    'optima/cat/optima-a30',
    'vector/cat/vector-10',
    'vector/cat/vector-15',
    'vector/cat/vector-20',
    'vector/cat/vector-30',
    'vector/cat/vector-drop',
    'vector/cat/vector-drop-ltd',
  ],
};

const generateUrls = (markets, resources) => {
  const urls = [];
  for (const market of markets) {
    urls[market.locale] = [];
    for (const category of categories) {
      for (const resource of resources[category]) {
        urls[market.locale].push(`${market.domain[category]}/${resource}`);
      }
    }
  }
  return urls;
};

const urls = generateUrls(markets, resources);

module.exports = urls;
