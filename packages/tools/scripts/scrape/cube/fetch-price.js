const scrape = require('@tools/scraper');

const scrapeHtml = (html) => {
  const [amount, currency] = html.trim().split(' ');
  return {
    amount: parseInt(amount),
    currency: currency,
  };
};

const fetchPrice = async (url) => {
  return await scrape.scrapeUrl(url, scrapeHtml);
};

module.exports = fetchPrice;
