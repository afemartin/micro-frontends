const categories = ['road', 'mountain', 'city'];

const markets = [
  {
    locale: 'en-GB',
    domain: {
      road: 'https://www.cube.eu/uk/2020/bikes/road/road',
      mountain: 'https://www.cube.eu/uk/2020/bikes/mountainbike/hardtail',
      city: 'https://www.cube.eu/uk/2020/bikes/urban',
    },
  },
  {
    locale: 'es-ES',
    domain: {
      road: 'https://www.cube.eu/es/2020/bikes/road/road-race',
      mountain: 'https://www.cube.eu/es/2020/bikes/mountainbike/hardtail',
      city: 'https://www.cube.eu/es/2020/bikes/urban',
    },
  },
  {
    locale: 'fr-FR',
    domain: {
      road: 'https://www.cube.eu/fr/2020/bikes/road/road-race',
      mountain: 'https://www.cube.eu/fr/2020/bikes/mountainbike/hardtail',
      city: 'https://www.cube.eu/fr/2020/bikes/urban',
    },
  },
  {
    locale: 'it-IT',
    domain: {
      road: 'https://www.cube.eu/it/2020/bikes/road/road-race',
      mountain: 'https://www.cube.eu/it/2020/bikes/mountainbike/hardtail',
      city: 'https://www.cube.eu/it/2020/bikes/mountainbike/hardtail',
    },
  },
];

const resources = {
  road: [
    'agree/cube-agree-c62-race-carbonnwhite-2020',
    'agree/cube-agree-c62-sl-carbonnflashyellow-2020',
    'agree/cube-agree-c62-slt-carbonnblack-2020',
    'attain/cube-attain-greynflashyellow-2020',
    'attain/cube-attain-gtc-race-carbonnflashyellow-2020',
    'attain/cube-attain-gtc-sl-carbonnwhite-2020',
    'attain/cube-attain-pro-blacknorange-2020',
    'attain/cube-attain-race-blacknwhite-2020',
    'attain/cube-attain-sl-blackngrey-2020',
    'litening/cube-litening-c68x-pro-carbonnwhite-2020',
    'litening/cube-litening-c68x-race-teamline-2020',
    'litening/cube-litening-c68x-sl-carbonnred-2020',
    'litening/cube-litening-c68x-slt-carbonnblue-2020',
    'sl-road/cube-sl-road-pro-iridiumnblue-2020',
    'sl-road/cube-sl-road-race-iridiumngreen-2020',
    'sl-road/cube-sl-road-redngrey-2020',
    'sl-road/cube-sl-road-sl-greynblue-2020',
  ],
  mountain: [
    'acid/cube-acid-iridiumnblack-2020',
    'aim/cube-aim-allroad-blacknwhite-2020',
    'aim/cube-aim-blacknred-2020',
    'aim/cube-aim-pro-blacknorange-2020',
    'aim/cube-aim-race-blacknflashgreen-2020',
    'aim/cube-aim-sl-allroad-blacknblue-2020',
    'aim/cube-aim-sl-iridiumnred-2020',
    'analog/cube-analog-blacknflashyellow-2020',
    'attention/cube-attention-greyngreen-2020',
    'attention/cube-attention-sl-blacknblue-2020',
    'elite/cube-elite-c68x-race-carbonnglossy-2020',
    'elite/cube-elite-c68x-sl-teamline-2020',
    'elite/cube-elite-c68x-slt-carbonnblue-2020',
    'flying-circus/cube-flying-circus-petrolnblack-2020',
    'nutrail/cube-nutrail-greennorange-2020',
    'reaction/cube-race-one-blackngreen-2020',
    'reaction/cube-reaction-c62-pro-carbonnblue-2020',
    'reaction/cube-reaction-c62-race-greynorange-2020',
    'reaction/cube-reaction-c62-sl-greybluengreen-2020',
    'reaction/cube-reaction-c62-slt-carbonnsilver-2020',
    'reaction/cube-reaction-pro-blacknflashyellow-2020',
    'reaction/cube-reaction-race-blacknorange-2020',
    'reaction/cube-reaction-tm-greennblack-2020',
  ],
  city: [
    'editor/cube-editor-blacknpurple-2020',
    'ella/cube-ella-cruise-oldbluenblue-2020-easy-entry',
    'ella/cube-ella-ride-greenncream-2020-easy-entry',
    'hyde/cube-hyde-blacknyellow-2020',
    'hyde/cube-hyde-pro-blacknblue-2020',
    'hyde/cube-hyde-race-blackngreen-2020',
    'town/cube-town-pro-greynwhite-2020',
  ],
};

const generateUrls = (markets, resources) => {
  const urls = [];
  for (const market of markets) {
    urls[market.locale] = [];
    for (const category of categories) {
      for (const resource of resources[category]) {
        urls[market.locale].push(`${market.domain[category]}/${resource}`);
      }
    }
  }
  return urls;
};

const urls = generateUrls(markets, resources);

module.exports = urls;
