const cheerio = require('cheerio');

const downloadImage = require('./download-image');
const fetchPrice = require('./fetch-price');
const fetchColor = require('./fetch-color');

const capitalize = (s) => {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
};

const getCategoryFromUrl = (url) => {
  const translatedCategory = url.split('/')[6];
  switch (translatedCategory) {
    case 'road':
      return 'road';
    case 'mountainbike':
      return 'mountain';
    case 'urban':
      return 'city';
    default:
      return null;
  }
};

const scrapeHtml = async (html) => {
  const $ = cheerio.load(html);

  const articleNumber = $('.bike-specs ul.js-short-specs>li')
    .eq(-2)
    .find('.value')
    .text()
    .trim();

  if (!articleNumber) return null;

  const sku = articleNumber.slice(0, 4);
  const csku = articleNumber.slice(0, 4) + '-' + articleNumber.slice(4, 6);

  const price = await fetchPrice(
    `https://www.cube.eu/index.php?id=1519&sbp=${articleNumber}&no_cache=1`
  );

  const url = $('meta[property="og:url"]').attr('content');

  const country = $('.language-switch .lang').text();
  const language = $('meta[name="language"]').attr('content');

  const color = {
    id: csku,
    name: $('.bike-specs ul.js-short-specs>li')
      .eq(-5)
      .find('.value')
      .text()
      .trim(),
    image: await downloadImage(
      csku,
      $('.stage-image a#bike-stage-image-light').attr('href')
    ),
    sizes: $('.bike-specs ul.js-long-specs>li')
      .eq(1)
      .find('.value')
      .text()
      .trim()
      .split(', ')
      .map((size) => {
        const ssku = `${csku}-${size}`;
        return {
          id: ssku,
          size: size,
          stock: true, // this variable could be both a boolean or an integer if there is specific information about the stock available
        };
      }),
  };

  const colorVariants = $('.bike-color-variants a').length
    ? await Promise.all(
        $('.bike-color-variants a')
          .map(async (i, color) => await fetchColor($(color).attr('href')))
          .get()
      )
    : [];

  const product = {
    id: sku,
    name: `${$('.stage-title .wrapper h1').text()} ${$(
      '.stage-title .wrapper h2'
    ).text()}`.trim(),
    image: null,
    details: {
      category: getCategoryFromUrl(url),
      brand: 'Cube',
      family: $('.stage-title .wrapper h1').text(),
      model: $('.stage-title .wrapper h2').text() || null,
      year: parseInt(url.split('/')[4]),
      weight: $('.bike-specs ul.js-long-specs>li')
        .eq(-3)
        .find('.value')
        .text()
        .trim(),
    },
    price: price,
    components: $('.bike-specs ul.js-long-specs>li')
      .slice(0, $('.bike-specs ul.js-long-specs>li').length - 4)
      .map((i, component) => {
        return {
          name: capitalize(
            $(component)
              .find('.desc')
              .text()
              .trim()
          ),
          description: $(component)
            .find('.value')
            .text()
            .trim(),
        };
      })
      .get(),
    colors: [color, ...colorVariants],
    url,
    metadata: {
      country,
      language,
    },
  };

  // Set the main product image as the first color image
  product.image = { ...product.colors[0].image };

  return product;
};

module.exports = scrapeHtml;
