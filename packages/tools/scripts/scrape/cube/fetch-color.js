const cheerio = require('cheerio');

const scrape = require('@tools/scraper');

const downloadImage = require('./download-image');

const scrapeHtml = async (html) => {
  const $ = cheerio.load(html);

  const articleNumber = $('.bike-specs ul.js-short-specs>li')
    .eq(-2)
    .find('.value')
    .text()
    .trim();

  if (!articleNumber) return null;

  const csku = articleNumber.slice(0, 4) + '-' + articleNumber.slice(4, 6);

  return {
    id: csku,
    name: $('.bike-specs ul.js-short-specs>li')
      .eq(-5)
      .find('.value')
      .text()
      .trim(),
    image: await downloadImage(
      csku,
      $('.stage-image a#bike-stage-image-light').attr('href')
    ),
    sizes: $('.bike-specs ul.js-long-specs>li')
      .eq(1)
      .find('.value')
      .text()
      .trim()
      .split(', ')
      .map((size) => {
        const ssku = `${csku}-${size}`;
        return {
          id: ssku,
          size: size,
          stock: true, // this variable could be both a boolean or an integer if there is specific information about the stock available
        };
      }),
  };
};

const fetchColor = async (url) => {
  return await scrape.scrapeUrl(url, scrapeHtml);
};

module.exports = fetchColor;
