const fs = require('fs');
const jimp = require('jimp');
const path = require('path');

const downloadImage = async (id, urlStr) => {
  const targetDir = path.resolve(
    __dirname,
    '..',
    '..',
    '..',
    '..',
    'cdn',
    'images',
    'public'
  );

  const targetPath = path.join(targetDir, 'cube', id);

  if (
    !fs.existsSync(path.join(targetPath, 'large.jpg')) ||
    !fs.existsSync(path.join(targetPath, 'medium.jpg')) ||
    !fs.existsSync(path.join(targetPath, 'small.jpg')) ||
    !fs.existsSync(path.join(targetPath, 'thumbnail.jpg'))
  ) {
    fs.mkdirSync(targetPath, { recursive: true }, (err) => {
      if (err) throw err;
    });
    console.log('☁️ Downloading image from:', urlStr);
    return jimp
      .read(urlStr)
      .then((image) => {
        console.log(
          '️🖼 Generating large image:',
          path.join(targetPath, 'large.jpg')
        );
        image
          .clone()
          .background(0xffffffff)
          .contain(4100, 2418)
          .contain(1600, 1280)
          .crop(0, 0, 1600, 1200)
          .quality(80)
          .writeAsync(path.join(targetPath, 'large.jpg'));

        return image;
      })
      .then((image) => {
        console.log(
          '️🖼 Generating medium image:',
          path.join(targetPath, 'medium.jpg')
        );
        image
          .clone()
          .background(0xffffffff)
          .contain(4100, 2418)
          .contain(1600, 1280)
          .crop(0, 0, 1600, 1200)
          .scale(0.5)
          .quality(80)
          .writeAsync(path.join(targetPath, 'medium.jpg'));

        return image;
      })
      .then((image) => {
        console.log(
          '️🖼 Generating small image:',
          path.join(targetPath, 'small.jpg')
        );
        image
          .clone()
          .background(0xffffffff)
          .contain(4100, 2418)
          .contain(1600, 1280)
          .crop(0, 0, 1600, 1200)
          .scale(0.25)
          .quality(80)
          .writeAsync(path.join(targetPath, 'small.jpg'));

        return image;
      })
      .then((image) => {
        console.log(
          '️🖼 Generating thumbnail image:',
          path.join(targetPath, 'thumbnail.jpg')
        );
        image
          .clone()
          .background(0xffffffff)
          .contain(4100, 2418)
          .contain(1600, 1280)
          .crop(0, 0, 1600, 1200)
          .scale(0.125)
          .quality(80)
          .writeAsync(path.join(targetPath, 'thumbnail.jpg'));

        return image;
      })
      .then(() => {
        return {
          large: path.join('cube', id, 'large.jpg'),
          medium: path.join('cube', id, 'medium.jpg'),
          small: path.join('cube', id, 'small.jpg'),
          thumbnail: path.join('cube', id, 'thumbnail.jpg'),
        };
      })
      .catch((err) => {
        console.error(err);
      });
  } else {
    return Promise.resolve({
      large: path.join('cube', id, 'large.jpg'),
      medium: path.join('cube', id, 'medium.jpg'),
      small: path.join('cube', id, 'small.jpg'),
      thumbnail: path.join('cube', id, 'thumbnail.jpg'),
    });
  }
};

module.exports = downloadImage;
