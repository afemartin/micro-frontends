const fs = require('fs');
const path = require('path');

const scrape = require('@tools/scraper');

const urlsCube = require('./cube/urls');
const urlsOrbea = require('./orbea/urls');

const scrapeHtmlCube = require('./cube/scrape-html');
const scrapeHtmlOrbea = require('./orbea/scrape-html');

const locales = ['en-GB', 'es-ES', 'fr-FR', 'it-IT'];

(async () => {
  for (const locale of locales) {
    const outputFile = path.resolve(
      __dirname,
      '..',
      '..',
      '..',
      'apis',
      'store',
      'entities',
      'products',
      'scraped-data',
      `${locale}.json`
    );

    const productsCube = await scrape.scrapeUrls(
      urlsCube[locale],
      scrapeHtmlCube
    );
    const productsOrbea = await scrape.scrapeUrls(
      urlsOrbea[locale],
      scrapeHtmlOrbea
    );
    const products = [...productsCube, ...productsOrbea];

    fs.writeFile(
      outputFile,
      JSON.stringify(products, null, 2),
      'utf8',
      () => {}
    );
  }
})();
