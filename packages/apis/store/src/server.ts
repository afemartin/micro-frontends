import express from 'express';
import jsonGraphqlExpress from 'json-graphql-server';
import data from '../entities';

const app = express();

const prefixes = ['/en-gb', '/es-es', '/fr-fr', '/it-it'];

// Page routes
for (const prefix of prefixes) {
  const locale =
    {
      '/en-gb': 'en-GB',
      '/es-es': 'es-ES',
      '/fr-fr': 'fr-FR',
      '/it-it': 'it-IT',
    }[prefix] || 'en-GB';

  app.use(`${prefix}/`, jsonGraphqlExpress(data[locale]));
}

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`🚀 Store GraphQL API listening on port ${port}!`)
);
