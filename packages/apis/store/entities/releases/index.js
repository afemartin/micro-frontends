const products = require('../products');

const getProduct = (locale, id) => {
  return products[locale].find((product) => product.id === id);
};

const getReleases = (locale) => [
  getProduct(locale, 'K100'),
  getProduct(locale, 'K101'),
  getProduct(locale, 'K102'),
  getProduct(locale, 'K103'),
  getProduct(locale, '3761'),
  getProduct(locale, '3762'),
  getProduct(locale, '3763'),
  getProduct(locale, '3764'),
];

module.exports = {
  'en-GB': getReleases('en-GB'),
  'es-ES': getReleases('es-ES'),
  'fr-FR': getReleases('fr-FR'),
  'it-IT': getReleases('it-IT'),
};
