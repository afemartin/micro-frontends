const orders = [
  {
    id: '358fbe51-d0e6-4094-a993-5d5623eab83b',
    products: ['K100-G9-55', 'K103-GA-57'],
  },
  {
    id: '9e7480a8-0b76-414b-b1ec-31a5f07e0ea8',
    products: ['K101-GB-51'],
  },
];

module.exports = {
  'en-GB': orders,
  'es-ES': orders,
  'fr-FR': orders,
  'it-IT': orders,
};
