const orders = require('./orders');
const products = require('./products');
const recommendations = require('./recommendations');
const releases = require('./releases');

const getLocalizedData = (locale) => ({
  orders: orders[locale],
  products: products[locale],
  recommendations: recommendations[locale],
  releases: releases[locale],
});

const data = {
  'en-GB': getLocalizedData('en-GB'),
  'fr-FR': getLocalizedData('fr-FR'),
  'it-IT': getLocalizedData('it-IT'),
  'es-ES': getLocalizedData('es-ES'),
};

module.exports = data;
