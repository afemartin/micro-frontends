const products = require('../products');

const getProduct = (locale, id) => {
  return products[locale].find((product) => product.id === id);
};

const getRecommendations = (locale) => [
  {
    id: 'K100',
    products: [
      getProduct(locale, 'K101'),
      getProduct(locale, 'K102'),
      getProduct(locale, 'K103'),
    ],
  },
  {
    id: 'K101',
    products: [
      getProduct(locale, 'K100'),
      getProduct(locale, 'K102'),
      getProduct(locale, 'K103'),
    ],
  },
  {
    id: 'K102',
    products: [
      getProduct(locale, 'K100'),
      getProduct(locale, 'K101'),
      getProduct(locale, 'K103'),
    ],
  },
  {
    id: 'K103',
    products: [
      getProduct(locale, 'K100'),
      getProduct(locale, 'K101'),
      getProduct(locale, 'K102'),
    ],
  },
];

module.exports = {
  'en-GB': getRecommendations('en-GB'),
  'es-ES': getRecommendations('es-ES'),
  'fr-FR': getRecommendations('fr-FR'),
  'it-IT': getRecommendations('it-IT'),
};
