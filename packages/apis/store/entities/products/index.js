const productsEnGB = require('./scraped-data/en-GB');
const productsEsES = require('./scraped-data/es-ES');
const productsFrFR = require('./scraped-data/fr-FR');
const productsItIT = require('./scraped-data/it-IT');

module.exports = {
  'en-GB': productsEnGB,
  'es-ES': productsEsES,
  'fr-FR': productsFrFR,
  'it-IT': productsItIT,
};
