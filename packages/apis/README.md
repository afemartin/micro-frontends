# APIs

This section of the project will implement the REST apis that the frontend elements will consume.

There are several internal REST APIs (mocked with json-server) and one external GraphQL API (mocked with json-graphql-server).
