# UI

This section of the project will cover the UI framework used by all the frontend elements.

UPDATE: Since build a UI library it is a very complex and time consuming, this project will use [Semantic UI](https://semantic-ui.com) and the [Semantic UI React](https://react.semantic-ui.com) which is a good initial choice and also can serve as inspiration for a future implementation of a custom UI library.
