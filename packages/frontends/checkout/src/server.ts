import express from 'express';
import path from 'path';

import ReactDOMServer from 'react-dom/server';

import renderBasket from './basket/render';
import renderBuy from './buy/render';

const app = express();

// Fragment routes
app.get('/:locale(en-gb|es-es|fr-fr|it-it)/basket', (req: any, res: any) => {
  const [language, country] = req.params.locale.split('-');
  const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
  const props = { locale, count: 0 };
  const html = ReactDOMServer.renderToString(renderBasket(props));
  const data =
    '<script>' +
    `window.__CHECKOUT_BASKET_DATA__=${JSON.stringify(props)}` +
    '</script>';
  res.send(`${html}${data}`);
});
app.get('/:locale(en-gb|es-es|fr-fr|it-it)/buy', (req: any, res: any) => {
  const [language, country] = req.params.locale.split('-');
  const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
  const sku = req.query.sku;
  const props = { locale, sku };
  const html = ReactDOMServer.renderToString(renderBuy(props));
  const data =
    '<script>' +
    `window.__CHECKOUT_BUY_DATA__=${JSON.stringify(props)}` +
    '</script>';
  res.send(`${html}${data}`);
});

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'dist')));

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`🚀 Checkout Frontend listening on port ${port}!`)
);
