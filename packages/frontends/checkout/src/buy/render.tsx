import I18n, { I18nProvider} from '@tools/react-i18n';
import React from 'react';
import { Button } from 'semantic-ui-react';

interface BaseProps {
  locale: string;
}

// tslint:disable-next-line:no-empty-interface
interface BuyProps extends BaseProps {}

const renderBuy: React.FC<BuyProps> = ({ locale }) => {
  const handleAddToCart = () => {
    (window as any).checkout.count += 1;
    window.dispatchEvent(
      new CustomEvent('checkout:basket:changed', {
        bubbles: true,
      })
    );
  };
  return (
    <I18nProvider language={locale.substring(0, 2)}>
      <Button fluid={true} primary={true} size="large" onClick={handleAddToCart}>
        <I18n id="product.add_to_cart" />
      </Button>
    </I18nProvider>
  );
};

export default renderBuy;
