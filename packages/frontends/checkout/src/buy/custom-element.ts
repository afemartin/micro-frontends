/* eslint-disable no-use-before-define, no-console, class-methods-use-this */
/* globals HTMLElement, window, CustomEvent */
import ReactDOM from 'react-dom';

import render from './render';

class CheckoutBuy extends HTMLElement {
  static get observedAttributes() {
    return ['sku'];
  }
  attributeChangedCallback() {
    this.render();
  }
  connectedCallback() {
    this.render();
  }
  render() {
    const sku = this.getAttribute('sku') || undefined;
    const data = (window as any).__CHECKOUT_BUY_DATA__;
    ReactDOM.hydrate(render({ ...data, sku }), this);
  }
}

export default CheckoutBuy;
