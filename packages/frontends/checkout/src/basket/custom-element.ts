/* eslint-disable no-use-before-define, no-console, class-methods-use-this */
/* globals HTMLElement, window, CustomEvent */
import ReactDOM from 'react-dom';

import render from './render';

class CheckoutBasket extends HTMLElement {
  connectedCallback() {
    this.refresh = this.refresh.bind(this);
    this.render();
    window.addEventListener('checkout:basket:changed', this.refresh);
  }
  disconnectedCallback() {
    window.removeEventListener('checkout:basket:changed', this.refresh);
  }
  render() {
    const count = (window as any).checkout.count;
    const data = (window as any).__CHECKOUT_BASKET_DATA__;
    ReactDOM.hydrate(render({ ...data, count }), this);
  }
  refresh() {
    this.render();
  }
}

export default CheckoutBasket;
