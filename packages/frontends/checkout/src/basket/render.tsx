import I18n, { I18nProvider } from '@tools/react-i18n';
import React from 'react';
import { Icon, Label } from 'semantic-ui-react';

interface BaseProps {
  locale: string;
}

interface BasketProps extends BaseProps {
  count?: number;
}

const renderBasket: React.FC<BasketProps> = ({ locale, count = 0 }) => {
  const color = count === 0 ? 'grey' : 'green';

  return (
    <I18nProvider language={locale.substring(0, 2)}>
      <Label color={color} size="large">
        <Icon name="cart" /> <I18n id="basket.items" params={[count]} />
      </Label>
    </I18nProvider>
  );
};

export default renderBasket;
