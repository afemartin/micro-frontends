/* globals window */
import CheckoutBasket from './basket/custom-element';
import CheckoutBuy from './buy/custom-element';

(window as any).checkout = { count: 0 };

window.customElements.define('checkout-basket', CheckoutBasket);
window.customElements.define('checkout-buy', CheckoutBuy);
