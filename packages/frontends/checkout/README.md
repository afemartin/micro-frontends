# Checkout

This is the **Checkout Team** fragments service, which will be responsible to provide the user a smooth purchase experience.

Checkout Team domain: http://localhost:3011

### Available fragments

- Basket: `/basket`
- Buy: `/buy?sku=sample-sku`

### How to use

#### Inside an App

Add in your Nunjucks template the following snippets:

##### Basket fragment

```html
<checkout-basket>
  <esi:include src="http://localhost:3011/{{ locale.toLowerCase() }}/basket" />
</checkout-basket>

<script src="http://localhost:3000/polyfill/document-register-element.min.js"></script>
<script src="http://localhost:3011/bundle.js" async></script>
```

##### Buy fragment

```html
<checkout-buy sku="{{ sku }}">
  <esi:include
    src="http://localhost:3011/{{ locale.toLowerCase() }}/buy?sku={{ sku }}"
  />
</checkout-buy>

<script src="http://localhost:3000/polyfill/document-register-element.min.js"></script>
<script src="http://localhost:3011/bundle.js" async></script>
```

#### Inside a Fragment

To use a fragment inside another fragment you can add the following package:

```
$ yarn add @tools/react-fragment
```

##### Basket fragment

```jsx
import Fragment from '@tools/react-fragment';

const render: () => {
  return (
    ...
    <Fragment locale={locale} team="checkout" name="basket" />
    ...
  );
}
```

##### Buy fragment

```jsx
import Fragment from '@tools/react-fragment';

const render: () => {
  return (
    ...
    <Fragment locale={locale} team="checkout" name="buy" params={{ sku }} />
    ...
  );
}
```
