# Navigation

This is the **Navigation Team** fragments service, which will be responsible to provide the user inspiration and recommendation to explore other products.

Navigation Team domain: http://localhost:3012

### Available fragments

- Recommendation: `/menu`

### How to use

#### Inside an App

Add in your Nunjucks template the following snippets:

##### Recommendation fragment

```html
<navigation-menu>
  <esi:include src="http://localhost:3014/{{ locale.toLowerCase() }}/menu" />
</navigation-menu>

<script src="http://localhost:3000/polyfill/document-register-element.min.js"></script>
<script src="http://localhost:3013/bundle.js" async></script>
```

#### Inside a Fragment

To use a fragment inside another fragment you can add the following package:

```
$ yarn add @tools/react-fragment
```

##### Recommendation fragment

```jsx
import Fragment from '@tools/react-fragment';

const render: () => {
  return (
    ...
    <Fragment locale={locale} team="navigation" name="menu" />
    ...
  );
}
```
