import React from 'react';
import {
  Button,
  Container,
  Icon,
  Menu,
} from 'semantic-ui-react';

// Tools
import Fragment from '@tools/react-fragment';
import I18n, { I18nProvider } from '@tools/react-i18n';

// Children components
import MarketSwitcher from "./components/MarketSwitcher";

interface BaseProps {
  locale: string;
}

// tslint:disable-next-line:no-empty-interface
interface MenuProps extends BaseProps {}

const renderMenu: React.FC<MenuProps> = ({ locale }) => {
  return (
    <I18nProvider language={locale.substring(0, 2)}>
      <Menu fixed="top" inverted={true}>
        <Container>
          <Menu.Item as="a" href={`/${locale.toLowerCase()}`} header={true}>
            <Icon name="bicycle" size="big" /> <I18n id="store.brand" />
          </Menu.Item>
          <Menu.Item as="a" href={`/${locale.toLowerCase()}/category/road`}>
            <I18n id="category.road" />
          </Menu.Item>
          <Menu.Item as="a" href={`/${locale.toLowerCase()}/category/mountain`}>
            <I18n id="category.mountain" />
          </Menu.Item>
          <Menu.Item as="a" href={`/${locale.toLowerCase()}/category/city`}>
            <I18n id="category.city" />
          </Menu.Item>
          <MarketSwitcher locale={locale} />
          <Menu.Menu position="right">
            <Menu.Item>
              <Fragment locale={locale} team="checkout" name="basket" />
            </Menu.Item>
            <Menu.Item>
              <Button primary={true}><I18n id="customer.sign_in" /></Button>
            </Menu.Item>
          </Menu.Menu>
        </Container>
      </Menu>
    </I18nProvider>
  );
};

export default renderMenu;
