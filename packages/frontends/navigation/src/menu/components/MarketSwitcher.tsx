import React from 'react';
import { Dropdown, Flag, Icon } from 'semantic-ui-react';

import { getI18nText } from "@tools/react-i18n/src";

interface Market {
  country: string;
  currency: string;
  locale: string;
  url: string;
}

interface MarketSwitcherUiProps {
  otherMarkets: Market[];
  selectedMarket: Market;
}

interface MarketSwitcherContainerProps {
  locale: string;
}

const MarketSwitcherUi: React.FC<MarketSwitcherUiProps> = (props) => {
  const { otherMarkets, selectedMarket } = props;

  return (
    <Dropdown
      aria-label={getI18nText("a11y.select_market")}
      item={true}
      trigger={
        <>
          {/*
           // @ts-ignore */}
          <Flag name={selectedMarket.country} /> {/*
           // @ts-ignore */}
          <Icon name={selectedMarket.currency} />
        </>
      }
    >
      <Dropdown.Menu>
        {otherMarkets.map((market) => (
          <Dropdown.Item key={market.locale} as={'a'} href={market.url}>
            {/*
           // @ts-ignore */}
            <Flag name={market.country} /> {/*
           // @ts-ignore */}
            <Icon name={market.currency} />
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  );
};

const MarketSwitcherContainer: React.FC<MarketSwitcherContainerProps> = (
  props
) => {
  const { locale } = props;

  // React state
  const [currentLocation, setCurrentLocation] = React.useState<string>('');

  // React hooks
  React.useEffect(() => { setCurrentLocation(window.location.href)}, []);

  // TODO: move this constant value to a unified configuration or a new graphql entity
  const availableMarkets = [
    {
      country: 'uk',
      currency: 'pound',
      locale: 'en-GB',
      url: currentLocation.replace(locale.toLowerCase(), 'en-gb'),
    },
    {
      country: 'es',
      currency: 'euro',
      locale: 'es-ES',
      url: currentLocation.replace(locale.toLowerCase(), 'es-es'),
    },
    {
      country: 'it',
      currency: 'euro',
      locale: 'it-IT',
      url: currentLocation.replace(locale.toLowerCase(), 'it-it'),
    },
    {
      country: 'fr',
      currency: 'euro',
      locale: 'fr-FR',
      url: currentLocation.replace(locale.toLowerCase(), 'fr-fr'),
    },
  ];

  const otherMarkets = availableMarkets.filter(
    (market) => market.locale !== locale
  );

  const selectedMarket = availableMarkets.find(
    (market) => market.locale === locale
  );

  return (
    <MarketSwitcherUi
      otherMarkets={otherMarkets}
      selectedMarket={selectedMarket}
    />
  );
};

export default MarketSwitcherContainer;
