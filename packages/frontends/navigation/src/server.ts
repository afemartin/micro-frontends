import express from 'express';
import path from 'path';

import ReactDOMServer from 'react-dom/server';

import renderMenu from './menu/render';

const app = express();

// Fragment routes
app.get('/:locale(en-gb|es-es|fr-fr|it-it)/menu', (req, res) => {
  const [language, country] = req.params.locale.split('-');
  const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
  const props = { locale };
  const html = ReactDOMServer.renderToString(renderMenu(props));
  const data =
    '<script>' +
    `window.__NAVIGATION_MENU_DATA__=${JSON.stringify(props)}` +
    '</script>';
  res.send(`${html}${data}`);
});

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'dist')));

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`🚀 Navigation Frontend listening on port ${port}!`)
);
