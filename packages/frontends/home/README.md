# Home

This is the **Home Team** fragments service, which will be responsible to provide the user inspiration and recommendation to explore other products.

Home Team domain: http://localhost:3012

### Available fragments

- Releases: `/releases`

### How to use

#### Inside an App

Add in your Nunjucks template the following snippets:

##### Recommendation fragment

```html
<home-releases>
  <esi:include
    src="http://localhost:3013/{{ locale.toLowerCase() }}/releases"
  />
</home-releases>

<script src="http://localhost:3000/polyfill/document-register-element.min.js"></script>
<script src="http://localhost:3013/bundle.js" async></script>
```

#### Inside a Fragment

To use a fragment inside another fragment you can add the following package:

```
$ yarn add @tools/react-fragment
```

##### Recommendation fragment

```jsx
import Fragment from '@tools/react-fragment';

const render: () => {
  return (
    ...
    <Fragment locale={locale} team="home" name="releases" />
    ...
  );
}
```
