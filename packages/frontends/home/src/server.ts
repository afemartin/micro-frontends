import express from 'express';
import path from 'path';

import ReactDOMServer from 'react-dom/server';

import renderReleases from './releases/render';

import { fetchReleases } from './releases/services';

const app = express();

// Fragment routes
app.get('/:locale(en-gb|es-es|fr-fr|it-it)/releases', (req, res) => {
  const [language, country] = req.params.locale.split('-');
  const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
  fetchReleases(locale).then((releases) => {
    const props = { locale, releases };
    const html = ReactDOMServer.renderToString(renderReleases(props));
    const data =
      '<script>' +
      `window.__HOME_RELEASES_DATA__=${JSON.stringify(props)}` +
      '</script>';
    res.send(`${html}${data}`);
  });
});

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'dist')));

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`🚀 Home Frontend listening on port ${port}!`)
);
