/* eslint-disable no-use-before-define, no-console, class-methods-use-this */
/* globals HTMLElement, window, CustomEvent */
import ReactDOM from 'react-dom';

import render from './render';

class CustomElement extends HTMLElement {
  connectedCallback() {
    this.render();
  }
  render() {
    const data = (window as any).__HOME_RELEASES_DATA__;
    ReactDOM.hydrate(render({ ...data }), this);
  }
}

export default CustomElement;
