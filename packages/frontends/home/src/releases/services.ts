import fetch from 'isomorphic-unfetch';

export const fetchReleases = (locale: string) =>
  fetch(`http://localhost:3030/${locale.toLowerCase()}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{
        allReleases {
          id,
          name,
          image,
          details,
          price
        }
      }`,
    }),
  })
    .then((response) => {
      return response.json();
    })
    .then((responseAsJson) => {
      return responseAsJson.data.allReleases;
    });
