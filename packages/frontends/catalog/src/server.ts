import express from 'express';
import path from 'path';

import ReactDOMServer from 'react-dom/server';

import renderList from './list/render';

import { fetchProducts } from './list/services';

const app = express();

// Fragment routes
app.get('/:locale(en-gb|es-es|fr-fr|it-it)/list', (req, res) => {
  const [language, country] = req.params.locale.split('-');
  const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
  const category = req.query.category;
  fetchProducts(locale, category).then((products) => {
    const props = { locale, products, category };
    const html = ReactDOMServer.renderToString(renderList(props));
    const data =
      '<script>' +
      `window.__CATALOG_LIST_DATA__=${JSON.stringify(props)}` +
      '</script>';
    res.send(`${html}${data}`);
  });
});

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'dist')));

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`🚀 Catalog Frontend listening on port ${port}!`)
);
