import I18n, { I18nProvider } from '@tools/react-i18n';
import React from 'react';
import { Card, Container, Grid, Image } from 'semantic-ui-react';

// TODO: figure out a way to share Types without importing them from another team service
import { Product } from '../../../product/src/details/types';

interface BaseProps {
  locale: string;
}

type Category = 'road' | 'mountain' | 'city';

interface ListProps extends BaseProps {
  sku?: string;
  products: Product[];
  category: Category;
}

const renderList: React.FC<ListProps> = ({ locale, products, category }) => {
  return (
    <I18nProvider language={locale.substring(0, 2)}>
      <Container>
        <h1><I18n id={`category.${category}`} /></h1>
        <Grid>
          {products.map((product) => {
            const priceFormatted = product.price.amount.toLocaleString(locale, {
              style: 'currency',
              currency: product.price.currency,
            });
            return (
              <Grid.Column key={product.id} width={4}>
                <Card
                  href={`/${locale.toLowerCase()}/product/${product.id}`}
                  image={
                    <Image
                      alt={product.name}
                      src={`http://localhost:3003/${product.image.small}`}
                    />
                  }
                  header={`${product.details.brand} - ${product.name}`}
                  description={priceFormatted}
                />
              </Grid.Column>
            );
          })}
        </Grid>
      </Container>
    </I18nProvider>
  );
};

export default renderList;
