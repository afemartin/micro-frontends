import fetch from 'isomorphic-unfetch';

// TODO: Modify the query to filter by category once category has been added to the mocked data
export const fetchProducts = (locale: string, category: string) =>
  fetch(`http://localhost:3030/${locale.toLowerCase()}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{
        allProducts {
          id,
          name,
          image,
          details,
          price
        }
      }`,
    }),
  })
    .then((response) => {
      return response.json();
    })
    .then((responseAsJson) => {
      // XXX: Since GraphQL JSON Server cannot filter inside nested fields
      // we have to filter here after fetching the data
      return responseAsJson.data.allProducts.filter(
        (product) => product.details.category === category
      );
    });
