/* eslint-disable no-use-before-define, no-console, class-methods-use-this */
/* globals HTMLElement, window, CustomEvent */
import ReactDOM from 'react-dom';

import render from './render';

class CustomElement extends HTMLElement {
  static get observedAttributes() {
    return ['category'];
  }
  attributeChangedCallback() {
    this.render();
  }
  connectedCallback() {
    this.render();
  }
  render() {
    const category = this.getAttribute('category') || undefined;
    const data = (window as any).__CATALOG_LIST_DATA__;
    ReactDOM.hydrate(render({ ...data, category }), this);
  }
}

export default CustomElement;
