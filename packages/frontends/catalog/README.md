# Catalog

This is the **Catalog Team** fragments service, which will be responsible to provide the users a way to find the products they are looking for.

Catalog Team domain: http://localhost:3012

### Available fragments

- List: `/list`

### How to use

#### Inside an App

Add in your Nunjucks template the following snippets:

##### List fragment

```html
<catalog-list category="{{ category }}">
  <esi:include
    src="http://localhost:3013/{{ locale.toLowerCase() }}/list?category={{ category }}"
  />
</catalog-list>

<script src="http://localhost:3000/polyfill/document-register-element.min.js"></script>
<script src="http://localhost:3013/bundle.js" async></script>
```

#### Inside a Fragment

To use a fragment inside another fragment you can add the following package:

```
$ yarn add @tools/react-fragment
```

##### List fragment

```jsx
import Fragment from '@tools/react-fragment';

const render: () => {
  return (
    ...
    <Fragment locale={locale} team="catalog" name="list" params={{ category }} />
    ...
  );
}
```
