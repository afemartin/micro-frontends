# Inspire

This is the **Inspire Team** fragments service, which will be responsible to provide the user inspiration and recommendation to explore other products.

Inspire Team domain: http://localhost:3012

### Available fragments

- Recommendations: `/recommendations?sku=sample-sku`

### How to use

#### Inside an App

Add in your Nunjucks template the following snippets:

##### Recommendations fragment

```html
<inspire-recommendations sku="{{ sku }}">
  <esi:include
    src="http://localhost:3012/{{ locale.toLowerCase() }}/recommendations?sku={{ sku }}"
  />
</inspire-recommendations>

<script src="http://localhost:3000/polyfill/document-register-element.min.js"></script>
<script src="http://localhost:3012/bundle.js" async></script>
```

#### Inside a Fragment

To use a fragment inside another fragment you can add the following package:

```
$ yarn add @tools/react-fragment
```

##### Recommendations fragment

```jsx
import Fragment from '@tools/react-fragment';

const render: () => {
  return (
    ...
    <Fragment locale={locale} team="inspire" name="recommendations" params={{ sku }} />
    ...
  );
}
```
