import express from 'express';
import path from 'path';

import ReactDOMServer from 'react-dom/server';

import renderRecommendations from './recommendations/render';

import { fetchRecommendations } from './recommendations/services';

const app = express();

// Fragment routes
app.get('/:locale(en-gb|es-es|fr-fr|it-it)/recommendations', (req, res) => {
  const [language, country] = req.params.locale.split('-');
  const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
  const sku = req.query.sku;
  fetchRecommendations(locale, sku).then((recommendations) => {
    const props = { locale, sku, recommendations };
    const html = ReactDOMServer.renderToString(renderRecommendations(props));
    const data =
      '<script>' +
      `window.__INSPIRE_RECOMMENDATIONS_DATA__=${JSON.stringify(props)}` +
      '</script>';
    res.send(`${html}${data}`);
  });
});

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'dist')));

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`🚀 Inspire Frontend listening on port ${port}!`)
);
