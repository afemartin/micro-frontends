import fetch from 'isomorphic-unfetch';

export const fetchRecommendations = (locale: string, id: string) =>
  fetch(`http://localhost:3030/${locale.toLowerCase()}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{
        Recommendation(id: "${id}") {
          products,
        }
      }`,
    }),
  })
    .then((response) => {
      return response.json();
    })
    .then((responseAsJson) => {
      return responseAsJson.data.Recommendation.products;
    });
