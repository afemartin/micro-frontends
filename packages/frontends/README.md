# Frontends

This section of the project consist on all the frontend fragment services that will compose all the pages needed.

### Frontend Teams

- [Catalog](./catalog/README.md)
- [Checkout](./checkout/README.md)
- [Home](./home/README.md)
- [Inspire](./inspire/README.md)
- [Navigation](./navigation/README.md)
- [Product](./product/README.md)
