# Product

This is the **Product Team** fragments service, which will be responsible to provide the user advice about the product in order to purchase with confidence.

Product Team domain: http://localhost:3010

### Available fragments

- Details: `/details?sku=sample-sku`

### How to use

#### Inside an App

Add in your Nunjucks template the following snippets:

##### Details fragment

```html
<product-details sku="{{ sku }}">
  <esi:include
    src="http://localhost:3012/{{ locale.toLowerCase() }}/details?sku={{ sku }}"
  />
</product-details>

<script src="http://localhost:3000/polyfill/document-register-element.min.js"></script>
<script src="http://localhost:3010/bundle.js" async></script>
```

#### Inside a Fragment

To use a fragment inside another fragment you can add the following package:

```
$ yarn add @tools/react-fragment
```

##### Details fragment

```jsx
import Fragment from '@tools/react-fragment';

const render: () => {
  return (
    ...
    <Fragment locale={locale} team="product" name="details" params={{ sku }} />
    ...
  );
}
```
