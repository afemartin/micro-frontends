import express from 'express';
import path from 'path';

import ReactDOMServer from 'react-dom/server';

import renderDetails from './details/render';

import { fetchProduct } from './details/services';

const app = express();

// Fragment routes
app.get('/:locale(en-gb|es-es|fr-fr|it-it)/details', (req: any, res: any) => {
  const [language, country] = req.params.locale.split('-');
  const locale = `${language.toLowerCase()}-${country.toUpperCase()}`;
  const sku = req.query.sku;
  fetchProduct(locale, sku).then((product) => {
    const props = { locale, sku, product };
    const html = ReactDOMServer.renderToString(renderDetails(props));
    const data =
      '<script>' +
      `window.__PRODUCT_DETAILS_DATA__=${JSON.stringify(props)}` +
      '</script>';
    res.send(`${html}${data}`);
  });
});

// Respond static files
app.use(express.static(path.join(__dirname, '..', 'dist')));

const port = process.env.PORT || 3000;

app.listen(port, () =>
  console.log(`🚀 Product Frontend listening on port ${port}!`)
);
