export interface Product {
  id: string;
  name: string;
  image: Image;
  details: any;
  components: Component[];
  colors: Color[];
  price: Price;
  url: string;
}

export interface Image {
  large: string;
  medium: string;
  small: string;
  thumbnail: string;
}

export interface Component {
  name: string;
  description: string;
}

export interface Color {
  id: string;
  name: string;
  image: Image;
  sizes: Size[];
  price: string;
}

interface Size {
  id: string;
  size: string;
  stock: number;
}

interface Price {
  amount: number;
  currency: string;
}
