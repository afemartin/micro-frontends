import React from 'react';
import { Image, Modal } from 'semantic-ui-react';

// Types
import { Product } from '../types';

interface GalleryProps {
  product: Product; // Fetched product data during SSR
}

const Gallery: React.FC<GalleryProps> = ({ product }) => {
  // React state
  const [isGalleryModalOpen, setIsGalleryModalOpen] = React.useState<boolean>(
    false
  );

  const handleClickImage = () => {
    setIsGalleryModalOpen(true);
  };
  const handleCloseModal = () => {
    setIsGalleryModalOpen(false);
  };

  return (
    <>
      <Image
        alt={product.name}
        src={`http://localhost:3003/${product.image.medium}`}
        onClick={handleClickImage}
        style={{ cursor: 'zoom-in' }}
      />
      <Modal
        closeIcon={true}
        size="fullscreen"
        open={isGalleryModalOpen}
        onClose={handleCloseModal}
      >
        <Modal.Content>
          <Image
            alt={product.name}
            src={`http://localhost:3003/${product.image.large}`}
            centered={true}
          />
        </Modal.Content>
      </Modal>
    </>
  );
};

export default Gallery;
