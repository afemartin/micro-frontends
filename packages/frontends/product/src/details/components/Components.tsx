import React from 'react';
import { Table } from 'semantic-ui-react';

// Tools
import I18n from '@tools/react-i18n';

// Types
import { Product } from '../types';

interface ComponentsProps {
  product: Product; // Fetched product data during SSR
}

const Components: React.FC<ComponentsProps> = ({ product }) => {
  return (
    <>
      <h2><I18n id="product.components" /></h2>
      <Table basic="very" striped={true}>
        <Table.Body>
          {product.components.map((component, i) => (
            <Table.Row key={i}>
              <Table.Cell><strong>{component.name}</strong></Table.Cell>
              <Table.Cell>{component.description}</Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    </>
  );
};

export default Components;
