import React from 'react';
import { Button, Grid, Image } from 'semantic-ui-react';

// Tools
import Fragment from '@tools/react-fragment';

const Size = ({ size, selectedSize, onClick }) => {
  const handleSelectSize = () => {
    onClick(size.id);
  };

  return (
    <Button
      basic={true}
      fluid={true}
      toggle={true}
      disabled={!size.stock}
      active={selectedSize === size.id}
      onClick={handleSelectSize}
    >
      {size.size}
    </Button>
  );
};

const CallToActions = ({ locale, sku, colors }) => {
  // React state
  const [selectedSize, setSelectedSize] = React.useState<string>('');

  const handleSelectSize = (size: string) => {
    setSelectedSize(size);
  };

  return (
    <>
      {/* TODO: Replace inline styles with atoms classes from the Design System */}
      <Grid style={{ marginBottom: '14px' }}>
        {colors.map((color) => (
          <Grid.Row key={color.id}>
            <Grid.Column width={4}>
              <Image
                alt={color.name}
                src={`http://localhost:3003/${color.image.thumbnail}`}
              />
            </Grid.Column>
            <Grid.Column width={12}>
              <Grid>
                <Grid.Row columns={4}>
                  {color.sizes.map((size) => (
                    <Grid.Column key={size.id} style={{ margin: '7px 0' }}>
                      <Size
                        size={size}
                        selectedSize={selectedSize}
                        onClick={handleSelectSize}
                      />
                    </Grid.Column>
                  ))}
                </Grid.Row>
              </Grid>
            </Grid.Column>
          </Grid.Row>
        ))}
      </Grid>
      <Fragment locale={locale} team="checkout" name="buy" params={{ sku }} />
    </>
  );
};

export default CallToActions;
