import fetch from 'isomorphic-unfetch';

export const fetchProduct = (locale: string, id: string) =>
  fetch(`http://localhost:3030/${locale.toLowerCase()}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      query: `{
        Product(id: "${id}") {
          id,
          name,
          image,
          details,
          components,
          colors,
          price,
        }
      }`,
    }),
  })
    .then((response) => {
      return response.json();
    })
    .then((responseAsJson) => {
      return responseAsJson.data.Product;
    });
