import React from 'react';
import { Container, Grid } from 'semantic-ui-react';

// Tools
import Fragment from '@tools/react-fragment';
import { I18nProvider } from '@tools/react-i18n/';

// Children components
import CallToActions from './components/CallToActions';
import Components from './components/Components';
import Gallery from './components/Gallery';

// Types
import { Product } from './types';

interface BaseProps {
  locale: string;
}

interface DetailsProps extends BaseProps {
  sku?: string;
  product?: Product; // Fetched product data during SSR
}

const renderDetails: React.FC<DetailsProps> = ({ locale, sku, product }) => {
  const priceFormatted = product.price.amount.toLocaleString(locale, {
    style: 'currency',
    currency: product.price.currency,
  });

  return product ? (
    <I18nProvider language={locale.substring(0, 2)}>
      <Container>
        <Grid>
          <Grid.Row columns={2}>
            <Grid.Column>
              <Gallery product={product} />
            </Grid.Column>
            <Grid.Column>
              <h1>{product.name}</h1>
              <h2>{priceFormatted}</h2>
              <CallToActions
                locale={locale}
                sku={sku}
                colors={product.colors}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={1}>
            <Grid.Column>
              <Components product={product} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={1}>
            <Grid.Column>
              <Fragment
                locale={locale}
                team="inspire"
                name="recommendations"
                params={{ sku }}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </I18nProvider>
  ) : (
    <pre>no product not found</pre>
  );
};

export default renderDetails;
