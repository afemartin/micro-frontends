const en = require('./languages/en.json');
const es = require('./languages/es.json');
const fr = require('./languages/fr.json');
const it = require('./languages/it.json');

module.exports = {
  en,
  es,
  fr,
  it,
};
