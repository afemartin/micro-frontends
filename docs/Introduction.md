# Introduction

This project it is a POC (Proof of Concept) of a possible implementation of the micro frontends architecture.

To simplify all the source code will be in this mono repository that leverage the [yarn workspaces](https://yarnpkg.com/lang/en/docs/workspaces/).

The workspaces are divided in categories:

- Apis: REST Apis (mocked with json-server) following the traditional backend micro services architecture.
- Apps: Web applications.
- Frontends: Frontend elements that will be used to compose the applications and will be deployed separately following the micro frontends architecture.
- Tools: Set of packages for both production utils and development tools.
- UI: Main UI library following the [atomic design](http://atomicdesign.bradfrost.com/).
